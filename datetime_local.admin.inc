<?php

/**
 * @file
 * Adds admin page and form for choosing datetime-local fallback settings.
 */

/**
 * Implements hook_menu().
 */
function datetime_local_menu() {

  $items = array(
    'admin/config/regional/datetime-local' => array(
      'title' => 'Datetime-local Settings',
      'description' => 'Settings for the datetime-local module.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('datetime_local_settings_form'),
      'access arguments' => array('administer site configuration'),
      'weight' => 10,
    ),
  );

  return $items;
}


/**
 * Implements hook_form().
 */
function datetime_local_settings_form($form, &$form_state) {

  // Add settings option for users to load a fallback js datepicker for browsers
  // that don't support the datetime-local input type natively.
  $form['datetime_local_fallback_setting'] = array(
    '#type' => 'checkbox',
    '#title' => t('Load fallback js datepicker'),
    '#default_value' => variable_get('datetime_local_fallback_setting'),
    '#description' => t("For browsers that don't support the datetime-local input type natively, load a fallback js datepicker."),
  );

  return (system_settings_form($form));
}
