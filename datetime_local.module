<?php

/**
 * @file
 * Main module file for adding a datetime-local form element.
 */

include_once 'datetime_local.admin.inc';

// Set datetime-local date format as a constant.
define('DATETIME_LOCAL_PHP_DATE_FORMAT', 'Y-m-d\TH:i:s');


/**
 * Implements hook_element_info().
 */
function datetime_local_element_info() {
  $types['datetime-local'] = array(
    '#input' => TRUE,
    '#autocomplete_path' => FALSE,
    '#process' => array('datetime_local_element_process'),
    '#element_validate' => array('datetime_local_element_validate'),
    '#theme' => 'datetime_local',
    '#theme_wrappers' => array('form_element'),
    '#value_callback' => 'datetime_local_element_value',
  );

  return $types;
}


/**
 * Process callback function for datetime-local form element.
 *
 * @param $element
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *
 * @param $form_state
 *   The $form_state array for the form this element belongs to.
 *
 * @return array
 *   The processed datetime-local element.
 */
function datetime_local_element_process($element, &$form_state, $complete_form) {

  $min = FALSE;
  $max = FALSE;
  $default = FALSE;

  // Check if the #step attribute is set.
  if (!empty($element['#step'])) {

    // If #step is not 'any' or a negative number, unset #step so the element
    // is still editable.
    if (strtolower($element['#step']) != 'any' && !is_numeric($element['#step'])
    || (is_numeric($element['#step']) && $element['#step'] < 0)) {
      unset($element['#step']);
    }
  }

  // Make sure whatever #min date format is entered, it gets converted
  // to the required datetime-local YYYY-MM-DDTHH:MM:SS format.
  if (!empty($element['#min'])) {
    $min = TRUE;
    $element['#min'] = datetime_local_convert_format($element['#min']);
  }

  // Make sure whatever #max date format is entered, it gets converted
  // to the required datetime-local YYYY-MM-DDTHH:MM:SS format.
  if (!empty($element['#max'])) {
    $max = TRUE;
    $element['#max'] = datetime_local_convert_format($element['#max']);
  }

  // Make sure whatever #default_value format is entered, it gets converted
  // to the required datetime-local YYYY-MM-DDTHH:MM:SS format.
  if (!empty($element['#default_value'])) {
    $default = TRUE;
    $element['#default_value'] = datetime_local_convert_format($element['#default_value']);
  }

  // Check if #min and #max are both set
  if ($min == TRUE && $max == TRUE) {

    // If the #min is greater than the #max, unset both so the element
    // is still editable.
    if (strtotime($element['#min']) > strtotime($element['#max'])) {
      unset($element['#min'], $element['#max']);
      $min = FALSE;
      $max = FALSE;
    }
  }

  // Check if the #default_value attribute is set
  if ($default == TRUE) {

    // Check if the #min is set
    if ($min == TRUE) {

      // If the #min is greater than the #default_value, unset #min so the
      // element is still editable.
      if (strtotime($element['#min']) > strtotime($element['#default_value'])) {
        unset($element['#min']);
        $min == FALSE;
      }
    }

    // Check if the #max is set
    if ($max == TRUE) {

      // If the #max is less than the #default_value, unset #max so the
      // element is still editable.
      if (strtotime($element['#max']) < strtotime($element['#default_value'])) {
        unset($element['#max']);
        $max = FALSE;
      }
    }
  }

  if (variable_get('datetime_local_fallback_setting') == TRUE) {
    // Add hidden input for JS fallback value submission.
    $element['datetime-local-fallback-iso-input'] = array(
      '#type' => 'hidden',
      '#default_value' => '',
      '#tree' => TRUE,
    );
  }

  return $element;
}


/**
 * Value callback function for datetime-local form element.
 *
 * @param $element
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *
 * @param $input
 *
 * @param $form_state
 *
 */
function datetime_local_element_value($element, &$form_state, $input = FALSE) {

  if ($input !== FALSE) {

    return $input;
  }
  elseif (!empty($element['#default_value'])) {
    // Make sure whatever the default value date format is, it gets
    // converted to the required YYYY-MM-DDTHH:MM:SS datetime-local format.
    return datetime_local_convert_format($element['#default_value']);
  }
  else {

    return $input;
  }
}


/**
 * Validate callback function for datetime-local form element.
 *
 * @param array $element
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *
 * @param $form_state
 *
 */
function datetime_local_element_validate($element, &$form_state) {

  $value = '';

  if (!empty($element['#value'])) {

    // If the fallback iso formatted date input is being used, use that value.
    if (!empty($form_state['values']['datetime-local-fallback-iso-input'])) {
      $value = $form_state['values']['datetime-local-fallback-iso-input'];

    }
    else {
      $value = date_create($element['#value'], date_default_timezone_object());
      $value = datetime_local_convert_format($value);
    }

    // Check if the #min attibute has been set.
    if (!empty($element['#min'])) {
      $min = $element['#min'];

      // Check if the value is less than the set #min value.
      if (strtotime($value) < strtotime($min)) {
        form_error($element, t('Date: %value is less than the min: %min.', array(
          '%value' => $value,
          '%min' => $min,
        )));
      }
    }

    // Check if the #max attibute has been set.
    if (!empty($element['#max'])) {
      $max = $element['#max'];

      // Check if the value is greater than the set #max value.
      if (strtotime($value) > strtotime($max)) {
        form_error($element, t('Date: %value is greater than the max: %max.', array(
          '%value' => $value,
          '%max' => $max,
        )));
      }
    }
  }

  form_set_value($element, $value, $form_state);

  return $element;
}


/**
 * Implements hook_theme().
 */
function datetime_local_theme() {
  return array(
    'datetime_local' => array(
      'render element' => 'element',
    ),
  );
}


/**
 * Returns HTML for an datetime-local form element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties include: #title, #value, #description, #min, #max, #required,
 *     #attributes, #step.
 *
 * @ingroup themeable
 */
function theme_datetime_local($variables) {

  $element = $variables['element'];
  $element['#attributes']['type'] = 'datetime-local';
  $fallback_input = '';

  if (variable_get('datetime_local_fallback_setting') == TRUE) {

    // Load hidden input to store datetime-local formatted date.
    $fallback_input = theme('hidden', array(
      'element' => $element['datetime-local-fallback-iso-input'])
    );

    // Load fallback js datepicker with dependencies.
    libraries_load('moment');
    drupal_add_library('system', 'ui', array('requires_jquery' => TRUE));
    drupal_add_library('system', 'ui.datepicker', array(
      'requires_jquery' => TRUE,
    ));
    libraries_load('jquery-ui-timepicker-addon');
    drupal_add_js(drupal_get_path('module', 'datetime_local') . '/js/datetime_local.timepicker.js', array(
      'group' => JS_THEME,
      'every_page' => TRUE,
      'requires_jquery' => TRUE,
      'scope' => 'header',
    ));
  }

  element_set_attributes($element, array('id', 'name', 'value', 'step', 'min', 'max'));
  _form_set_class($element, array('form-text', 'form-datetime-local'));

  $output = $fallback_input . '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output;
}


/**
 * Implements hook_libraries_info().
 */
function datetime_local_libraries_info() {
  $libraries = array();

  $libraries['jquery-ui-timepicker-addon'] = array(
    'name' => 'jQuery Timepicker Addon',
    'vendor url' => 'http://trentrichardson.com/examples/timepicker',
    'download url' => 'https://github.com/trentrichardson/jQuery-Timepicker-Addon/releases',
    'version arguments' => array(
      'file' => 'jquery-ui-timepicker-addon.min.js',
      'pattern' => '/jQuery Timepicker Addon - v(\d+\.+\d+)/',
      'lines' => 2,
    ),
    'files' => array(
      'js' => array(
        'jquery-ui-timepicker-addon.min.js',
      ),
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'js' => array(
            'jquery-ui-timepicker-addon.min.js',
          ),
        ),
      ),
      'source' => array(
        'files' => array(
          'js' => array(
            'jquery-ui-timepicker-addon.js',
          ),
        ),
      ),
    ),
  );

  return $libraries;

};


/**
 * Checks whether the passed value is a unix timestamp or other date format and
 * tries to convert to the DATETIME_LOCAL_DATE_FORMAT format the datetime-local
 * form element requires.
 *
 * @param $datetime
 *   A date as either a PHP date object, UNIX timestamp, or date string.
 *
 * @param $format
 *   A valid PHP date format: http://php.net/manual/en/function.date.php
 *
 * @return (string)
 *   The date as a string formatted as YYYY-MM-DDTHH:MM:SS (Y-m-d\TH:i:s).
 *
 */
function datetime_local_convert_format($datetime, $format = DATETIME_LOCAL_PHP_DATE_FORMAT) {

  if (is_numeric($datetime)) {

    return format_date($datetime, 'custom', $format, date_default_timezone());
  }
  elseif (date_is_date($datetime)) {

    return date_format($datetime, $format);
  }
  else {
    // Assume a datetime string and parse to a PHP date object.
    $datetime = date_create($datetime, date_default_timezone_object());
    return date_format($datetime, $format);
  }
}
