INTRODUCTION
------------

The datetime-local module adds support for the HTML5 datetime-local form element
to Drupal 7. Because native browser support for this form element type is still
being implement by major browsers, it also provides an option to load a
fallback JavaScript date/timepicker calendar widget for browsers without
support.

The fallback JavaScript widget uses the Drupal 7 jQuery datapicker plugin
along with a bundled version of the jQuery Timepicker Addon plugin by Trent
Richardson: https://github.com/trentrichardson/jQuery-Timepicker-Addon

This module only adds support for the datetime-local form element to
developers implementing it via code, similar to the Elements module:
https://www.drupal.org/project/elements

For more information on the HTML5 datatime-local form type see:
 * http://www.w3.org/TR/html-markup/input.datetime-local.html
 * http://www.aquim.com/web-article-240.html
 * http://html5doctor.com/html5-forms-input-types/

 * For a full description of the module, visit the project page:
   https://drupal.org/project/datetime_local

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/datetime_local


 REQUIREMENTS
 ------------

 This module requires the following modules:

  * jQuery Update (https://www.drupal.org/project/jquery_update)
	* Date (https://www.drupal.org/project/date)
  * Moment.js (https://www.drupal.org/project/moment)
	* jQuery Timepicker Addon
	(https://github.com/trentrichardson/jQuery-Timepicker-Addon/releases)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Download the latest version of thejQuery Timepicker Addon plugin from
   https://github.com/trentrichardson/jQuery-Timepicker-Addon/releases
   Unpack and rename the "dist" directory to "jquery-ui-timepicker-addon" and
	 place it inside the "sites/all/libraries" directory. Make sure the path to
	 the plugin file is:
	 "sites/all/libraries/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"


CONFIGURATION
-------------

 * Enable fallback JavaScript date/timepicker calendar in Configuration »
   Datetime-local Settings:

   - Enable the checkbox "Load fallback js datepicker" and click the submit
	   button.


USAGE
-----

To use the datatime-local form element, set the #type of the form element to
'datetime-local' when defining your form array. You can also define the #min,
#max, and #step attributes. Valid dates for these and the #default_value can be
a PHP date object, UNIX timestamp, or date string.


RECOMMENDED MODULES
-------------------

 * Elements (https://www.drupal.org/project/elements):
   When enabled, this adds support for additional HTML5 form elements such as:
	 tel, url, email, search, etc.


MAINTAINERS
-----------

Current maintainers:
  * PCateNumbersUSA - https://www.drupal.org/u/pcatenumbersusa

This project has been sponsored by:
  * NumbersUSA
