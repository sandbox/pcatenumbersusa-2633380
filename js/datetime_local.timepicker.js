
/**
 * @file
 * JavaScript init file for the datetime-local timepicker library.
 *
 * Attaches a Drupal behavior for the datetime-local module to load the
 * optional fallback JavaScript data/time picker.
 */

 (function ($) {

	"use strict";

  Drupal.behaviors.datetimeLocalTimePicker = {
    attach: function (context, settings) {

      var testdatepicker = document.createElement('input');

      // Set INPUT element to the type we're testing for.
      testdatepicker.setAttribute('type', 'datetime-local');

      // If browser doesn't support INPUT type="datetime-local" init the
      // fallback JavaScript.
      if (testdatepicker.type == 'text') {

        var min_value;
        var max_value;

        var fallback_date_format = 'yy-mm-dd';
        var fallback_separator = ' ';
        var fallback_time_format = 'hh:mmtt';

        var datepicker_format = 'YYYY-MM-DD hh:mma';
        var iso_date_format = 'YYYY-MM-DDTHH:mm:ss';

        $('input[type="datetime-local"]', context).once().val(function (index, value) {

          // Check if the there is a default value.
          if (typeof value !== typeof undefined && value !== false && value !== '') {

            // Set the hidden fallback input to the default value.
            var fallback_input = $(this).prev('input[type="hidden"][name="datetime-local-fallback-iso-input"]');
            fallback_input.val($(this).val());

            return moment(value, iso_date_format).format(datepicker_format);
          }
        }).attr('min', function (index, min_attribute) {

          // Check if the 'min' attribute is set and not empty.
          if (typeof min_attribute !== typeof undefined && min_attribute !== false && min_attribute !== '') {

            // Convert the 'min' attribute value to a Date object so it can be
            // used with the datetimepicker min settings.
            min_value = moment(min_attribute, iso_date_format).toDate();
          }
        }).attr('max', function (index, max_attribute) {

          // Check if the 'max' attribute is set and not empty.
          if (typeof max_attribute !== typeof undefined && max_attribute !== false && max_attribute !== '') {

            // Convert the 'max' attribute value to a Date object so it can be
            // used with the datetimepicker max settings.
            max_value = moment(max_attribute, iso_date_format).toDate();
          }
        });

        // Initialize the datetimepicker JS widget.
        $('input[type="datetime-local"]', context).datetimepicker({
          dateFormat: fallback_date_format,
          separator: fallback_separator,
          timeFormat: fallback_time_format,
          controlType: 'select',
          showSecond: false,
          showMillisec: false,
          showMicrosec: false,
          showTimezone: false,
          oneLine: true,
          minDate: min_value,
          maxDate: max_value,
          onSelect: function (selectedDateTime, inst) {
            var fallback_input = $(this).prev('input[type="hidden"][name="datetime-local-fallback-iso-input"]');
            fallback_input.val(moment($(this).val(), datepicker_format).add(moment().seconds(), 's').format(iso_date_format));
          }
        });
      }
    }
  };

})(jQuery);
